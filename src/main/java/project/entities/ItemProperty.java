package project.entities;
public interface ItemProperty<P> {
    String getPropertieName();
    P getPropertieValue();
}
